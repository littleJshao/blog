//引入
const koa =require('koa') // 实例化koa对象
const Router = require('koa-router') // 引入路由中间件
const db = require('./config/keys').mongoURI // 引入数据库连接信息
const mongoose = require('mongoose') // 引入数据库驱动插件
const bodyParser = require('koa-bodyparser') // 将请求的数据jason化的中间件
const passport = require('koa-passport')  // 权限认证
const cors = require("koa-cors") // 处理跨域

//实例化
const app = new koa()
const router = new Router()

app.use(bodyParser())
app.use(cors())

//引入路由
const users = require('./routes/api/users')
const profile = require('./routes/api/profile')
const posts = require('./routes/api/posts')
const article = require('./routes/api/article')
const weather = require('./routes/api/weather')
const tag = require('./routes/api/tag')
const cloudphoto = require('./routes/api/cloudphoto')
const notice = require('./routes/api/notice')
const leave = require('./routes/api/leave')

app.use(passport.initialize())
app.use(passport.session())

//回调到config文件中 passport.js
require('./config/passport')(passport)

//配置路由地址
router.use("/api/users",users)
router.use("/api/profile", profile)
router.use("/api/posts", posts)
router.use("/api/article",article)
router.use("/api/weather", weather)
router.use("/api/tag", tag)
router.use("/api/cloudphoto", cloudphoto)
router.use("/api/notice", notice)
router.use("/api/leave", leave)


app.use(router.routes()).use(router.allowedMethods())
const port = process.env.PORT || 5000

mongoose.set('useFindAndModify', false)
//连接数据库
mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log('数据库连接成功...');
  })
  .catch(err => {
    console.log('数据库连接失败：', err);
  })

app.listen(port,()=>{
  console.log(`serve started on ${port}`)
})