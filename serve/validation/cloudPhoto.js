const Validator = require('validator')
const isEmpty = require('./isEmpty')

module.exports = function validatePhoto(data) {
  let errors = {};
  data.title = !isEmpty(data.title) ? data.title : ''
  data.text = !isEmpty(data.text) ? data.text : ''

  if (Validator.isEmpty(data.title)) {
    errors.title = '相册标题不能为空'
  }
  if (!Validator.isLength(data.title, { min: 1, max: 20 })) {
    errors.title = '相册标题的长度不能小于1位且不能超过20位';
  }
  if (Validator.isEmpty(data.text)) {
    errors.text = '相册内容不能为空'
  }
  if (!Validator.isLength(data.text, { min: 1, max: 200 })) {
    errors.text = '相册内容的长度不能小于1位且不能超过200位';
  }
  if (data.photo.split(',').length > 1) {
    errors.photo = '最多传一张图片'
  }
  return {
    errors,
    isValid: isEmpty(errors)
  }
}