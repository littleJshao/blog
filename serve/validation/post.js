const Validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function validatePostInput(data) {
  let errors = {};

  data.text = !isEmpty(data.text) ? data.text : '';

  if (!Validator.isLength(data.text, { min: 1, max: 100 })) {
    errors.text = 'text的长度不能小于1位且不能超过100位';
  }

  if (Validator.isEmpty(data.text)) {
    errors.text = 'text不能为空';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
