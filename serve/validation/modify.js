const Validator = require('validator')
const isEmpty = require('./isEmpty')

module.exports = function validateModify(data) {
  let errors = {}

  data.name = !isEmpty(data.title) ? data.title : ''
  data.motto = !isEmpty(data.motto) ? data.motto : ''

  if (Validator.isEmpty(data.name)) {
    errors.title = '用户名字不能为空'
  }
  if (!Validator.isLength(data.name, { min: 1, max: 15 })) {
    errors.name = "名字的长度不能小于1位且不能超过15位"
  }
  if(data.motto) {
    if (!Validator.isLength(data.motto, { min: 1, max: 15 })) {
      errors.motto = "个人介绍的长度不能小于1位且不能超过15位"
    }
  }
}