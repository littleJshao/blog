const Validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function validateArtile(data){
  let errors = {};

  data.title = !isEmpty(data.title) ? data.title : ''
  data.text = !isEmpty(data.text) ? data.text : ''
  data.tags = !isEmpty(data.tags) ? data.tags : ''

  if(Validator.isEmpty(data.title)) {
    errors.title = '文章标题不能为空'
  }
  if (!Validator.isLength(data.title, { min: 1, max: 50 })) {
    errors.title = '文章标题的长度不能小于1位且不能超过50位';
  }
  if (Validator.isEmpty(data.profile)) {
    errors.profile = '文章简介不能为空'
  }
  if (!Validator.isLength(data.profile, { min: 1, max: 200 })) {
    errors.profile = '文章简介的长度不能小于1位且不能超过200位';
  }
  if(Validator.isEmpty(data.text)) {
    errors.text = '文章内容不能为空'
  }
  if(!data.tags.split().length) {
    errors.tags = '文章标签不能为空'
  }else {
    const arr = data.tags.split()
    if(arr.length > 2) {
      errors.tags = '文章标签最多只能选择两个 '
    }
  }
  if (data.coverImages.split(',').length > 6) {
    errors.coverImages = '封面图片最多传六张'
  }
  return {
    errors,
    isValid: isEmpty(errors)
  }
}