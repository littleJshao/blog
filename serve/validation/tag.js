const Validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function validateTag(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : '';

  if (!Validator.isLength(data.name, { min: 1, max: 10 })) {
    errors.text = '标签名的长度不能小于1位且不能超过10位';
  }

  if (Validator.isEmpty(data.name)) {
    errors.text = '标签名不能为空';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
}