const Validator = require('validator')
const isEmpty = require('./isEmpty')

module.exports = function validateRegisterInput(data) {
  let errors = {}
  data.name = !isEmpty(data.name) ? data.name : ''
  data.email = !isEmpty(data.email) ? data.email : ''
  data.password = !isEmpty(data.password) ? data.password : ''
  data.password2 = !isEmpty(data.password2) ? data.password2 : ''
  //如果名字处于在 <= 1 或 >= 15 如果名字正确则不需要处理。所以进行取反
  if (!Validator.isLength(data.name, { min: 1, max: 15 })) {
    errors.name = "名字的长度不能小于1位且不能超过15位"
  }
  if (Validator.isEmpty(data.name)) {
    errors.name = '名字不能为空';
  }
  if (Validator.isEmpty(data.email)) {
    errors.email = '邮箱不能为空';
  }
  if (!Validator.isEmail(data.email)) {
    errors.emali = "邮箱不合法"
  }
  if (Validator.isEmpty(data.password)) {
    errors.name = "密码不能为空"
  }
  if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = "密码长度限于6至30位" + data.password
  }
  if (Validator.isEmpty(data.password2)) {
    errors.password2 = "请填写确认密码"
  }
  if (!Validator.equals(data.password, data.password2)) {
    errors.password2 = "两次输入的密码不同"
  }
  return {
    errors,
    isValid: isEmpty(errors) // 错误信息是否为空
  }
}
