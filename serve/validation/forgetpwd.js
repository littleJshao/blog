const Validator = require('validator')
const isEmpty = require('./isEmpty')

module.exports = function validateRegisterInput(data) {
  let errors = {}
  data.email = !isEmpty(data.email) ? data.email : ''
  data.password = !isEmpty(data.password) ? data.password : ''
  data.password2 = !isEmpty(data.password2) ? data.password2 : ''
  if (Validator.isEmpty(data.email)) {
    errors.email = '邮箱不能为空';
  }
  if (!Validator.isEmail(data.email)) {
    errors.emali = "邮箱不合法"
  }
  if (Validator.isEmpty(data.password)) {
    errors.name = "密码不能为空"
  }
  if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = "密码长度限于6至30位" + data.password
  }
  if (Validator.isEmpty(data.password2)) {
    errors.password2 = "请填写确认密码"
  }
  if (!Validator.equals(data.password, data.password2)) {
    errors.password2 = "两次输入的密码不同"
  }
  return {
    errors,
    isValid: isEmpty(errors) // 错误信息是否为空
  }
}
