const Router = require('koa-router');
const router = new Router();
const passport = require('koa-passport')
const mongoose = require('mongoose')
//引入模板实例
const Photo = require('../../models/CloudPhoto')
//引入User
const User = require('../../models/User')

//引入验证
const validatePhoto = require('../../validation/cloudPhoto')
const isEmpty = require('../../validation/isEmpty')

/**
 * @route POST api/publishPhoto
 * @desc  发布图片
 * @access 私密
 */
router.post('/publish', passport.authenticate('jwt', { session: false }), async ctx => {
  if (isEmpty(ctx.request.body)) {
    ctx.status = 400
    ctx.body = '格式错误'
    return
  }
  const { errors, isValid } = validatePhoto(ctx.request.body)
  // 判断是否验证通过
  if (!isValid) {
    ctx.status = 400;
    ctx.body = errors;
    return;
  }

  const photo = {}
  photo.user = ctx.state.user.id // 发布者的id
  photo.photo = ctx.request.body.photo // 上传的相册
  photo.title = ctx.request.body.title // 内容标题
  photo.text = ctx.request.body.text // 对相册简要描述的内容

  await new Photo(photo).save().then(async res => {
    photo.id = res._id
    const user = await User.findById(ctx.state.user.id)
    const userPhotos = user.photos
    userPhotos.push(photo.id) // 将发布的云相册存入到数据库中 
    await User.findByIdAndUpdate(ctx.state.user.id, { $set: { photos: userPhotos } }, { new: true })
      .then(result => {
        ctx.status = 200
        ctx.body = {
          msg: '上传成功',
          data: res
        }
      }).catch(err => {
        ctx.status = 400
        ctx.body = err
      })
  })
})

/**
 * @route POST api/cloudphoto/like
 * @desc  点赞图片
 * @access 私密  
 */
router.post('/like', passport.authenticate('jwt', { session: false }), async ctx => {
  const photoId = ctx.request.body.pId
  const photo = await Photo.findById({ _id: mongoose.Types.ObjectId(photoId) })
  if (!photo) {
    ctx.status = 404
    ctx.body = {
      msg: '图片不存在'
    }
    return
  }
  const isLike = photo.likes.filter(user => user._id.toString() === ctx.state.user.id).length > 0
  if (isLike) {
    const removeIndex = photo.likes.map(item => item.toString()).indexOf(ctx.state.user.id)
    photo.likes.splice(removeIndex, 1)
    photo.likesNum -= 1
    await Photo.findOneAndUpdate(
      { _id: photoId },
      { $set: photo },
      { new: true }
    ).then(res => {
      ctx.status = 200
      ctx.body = {
        isLike: '取消喜欢',
        count: res.likesNum,
        likes: res.likes,
        liked: false
      }
    })
  } else {
    photo.likes.unshift(ctx.state.user.id)
    photo.likesNum += 1
    await Photo.findOneAndUpdate(
      { _id: photoId },
      { $set: photo },
      { new: true }
    ).then(res => {
      ctx.status = 200
      ctx.body = {
        isLike: '收到你的喜欢',
        count: res.likesNum,
        likes: res.likes,
        liked: true
      }
    })
  }
})

/**
 * @route POST api/cloudphoto/hotList
 * @desc  获取最热图片
 * @access 公开
 */
router.post('/hotList',async ctx => {
  const limit = Number(ctx.request.body.limit)
  const page = Number(ctx.request.body.page)
  const skip = limit * (page - 1)
  await Photo.find({}).sort({'likesNum': -1}).populate('user',['name'])
  .skip(skip)
  .limit(limit)
  .then(res => {
    ctx.status = 200
    const totalPage = Math.ceil((res.length + limit - 1)/limit)
    ctx.body = {
      mag:'获取成功',
      list: res,
      limit: limit,
      page: page,
      totalPage: totalPage
    }
  })
})
/**
 * @route POST api/cloudphoto/hotList
 * @desc  获取最新图片
 * @access 公开
 */
router.post('/newList', async ctx => {
  const limit = Number(ctx.request.body.limit)
  const page = Number(ctx.request.body.page)
  const skip = limit * (page - 1)
  await Photo.find({}).sort({ 'date': -1 }).populate('user', ['name'])
    .skip(skip)
    .limit(limit)
    .then(res => {
      ctx.status = 200
      const totalPage = Math.ceil((res.length + limit - 1) / limit)
      ctx.body = {
        mag: '获取成功',
        list: res,
        limit: limit,
        page: page,
        totalPage: totalPage
      }
    })
})
/**
 * @route GET api/cloudphoto/myPhotos
 * @desc  获取个人的图片
 * @access 私密
 */
router.get('/myPhotos', passport.authenticate('jwt', { session: false }), async ctx => {
  await Photo.find({}).where('user').eq(ctx.state.user.id).populate('user', ['name'])
  .then(res => {
    ctx.status = 200
    ctx.body = res
  })
})

//搜索相册
router.post('/photo-search', async ctx => {
  const limit = Number(ctx.request.body.limit)
  const page = Number(ctx.request.body.page)
  const skip = limit * (page - 1)
  const keyword = ctx.request.body.keyword
  const reg = new RegExp(keyword, 'i')
  const pageCount = await Photo.count({
    $or: [{ title: { $regex: reg } }]
  })
  await Photo.find(
    {
      $or: [{ title: { $regex: reg } }]
    }
  ).populate('user', ['name']).skip(skip)
    .limit(limit)
    .then(res => {
      ctx.status = 200
      ctx.body = {
        data: res,
        limit: limit,
        page: page,
        pageTotal: pageCount
      }
    })
})
module.exports = router.routes()