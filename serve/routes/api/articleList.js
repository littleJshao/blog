const Router = require('koa-router');
const router = new Router();
const passport = require('koa-passport')
const mongoose = require('mongoose')

//引入模板实例
const Article = require('../../models/article')
//引入User
const User = require('../../models/User')
//引入验证
const validateArtile = require('../../validation/article')
const isEmpty = require('../../validation/isEmpty')

/**
 * @route GET api/article/indexList
 * @desc  发布文章
 * @access 接口是公开的
 */
