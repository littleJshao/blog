const Router = require('koa-router');
const router = new Router();
const passport = require('koa-passport')
const mongoose = require('mongoose')
//引入模板实例
const Article = require('../../models/article')
//引入User
const User = require('../../models/User')
const Tag = require('../../models/Tag')

//引入验证
const validateArtile = require('../../validation/article')
const isEmpty = require('../../validation/isEmpty')

/**
 * @route POST api/article/publish
 * @desc  发布文章
 * @access 接口是公开的
 */
router.post('/publish', passport.authenticate('jwt', { session: false }), async ctx => {
  if (isEmpty(ctx.request.body)) {
    ctx.status = 400
    ctx.body = '格式错误'
    return
  }
  const { errors, isValid } = validateArtile(ctx.request.body)
  // 判断是否验证通过
  if (!isValid) {
    ctx.status = 400;
    ctx.body = errors;
    return;
  }

  // 写入文章信息
  const article = {}
  article.user = ctx.state.user.id
  article.title = ctx.request.body.title
  article.text = ctx.request.body.text
  article.profile = ctx.request.body.profile
  article.coverImages = ctx.request.body.coverImages.split(',')
  article.tags = []
  let tags = ctx.request.body.tags.split(',') // 因为存取的是objectId 要特殊处理
  for (let item of tags) {
    article.tags.push(mongoose.Types.ObjectId(item))
  }
  await new Article(article).save().then(async (res) => {
    article.id = res._id // 获取文章的id
    // 将发布的文章的id 存入到对应的用户中
    const findUser = await User.findById(ctx.state.user.id)
    const userArticle = findUser.article // 获取用户现有的文章
    userArticle.push(article.id)
    await User.findByIdAndUpdate(ctx.state.user.id, { $set: { article: userArticle } })
    ctx.status = 200
    ctx.body = res
  }).catch(err => {
    ctx.status = 400
    ctx.body = err
  })
})

//更新文章
router.post('/update', async ctx => {
  const artId = ctx.request.body.artId
  const article = {}
  article.user = ctx.state.user.id
  article.title = ctx.request.body.title
  article.text = ctx.request.body.text
  article.profile = ctx.request.body.profile
  article.coverImages = ctx.request.body.coverImages.split(',')
  article.tags = []
  let tags = ctx.request.body.tags.split(',') // 因为存取的是objectId 要特殊处理
  for (let item of tags) {
    article.tags.push(mongoose.Types.ObjectId(item))
  }
  await new Article().findByIdAndUpdate({ _id: artId},{$set:{article}}).then(async (res) => {
    article.id = res._id // 获取文章的id
    // 将发布的文章的id 存入到对应的用户中
    ctx.status = 200
    ctx.body = res
  }).catch(err => {
    ctx.status = 400
    ctx.body = err
  })
})

/**
 * @route GET api/article/list
 * @desc  获取全部文章
 * @access 接口是公开的
 */
router.post('/list', async ctx => {
  const limit = Number(ctx.request.body.limit)
  const page = Number(ctx.request.body.page)
  const skip = limit * (page - 1)
  await Article.find({ audit: { $eq: 1 } }).populate('user', ['_id', 'name', 'avatar', 'motto', 'followed']).populate('tags', ['name'])
    .skip(skip)
    .limit(limit)
    .then(res => {
      // const hotArticle = res.filter(i => i.isHot)
      // const common = res.filter(i => !i.isHot)
      // const arr = [...hotArticle, ...common]
      ctx.status = 200
      ctx.body = res
    })
})

/**
 * @route GET api/article/detail
 * @desc  获取一篇文章详情
 * @access 接口是公开的
 */
router.get('/detail', async ctx => {
  await Article.findById({ _id: ctx.request.query.articleId })
  .populate('user', ['name', 'avatar'])
  .populate('tags', ['name']).then(res => {
    ctx.status = 200
    ctx.body = res
  })
})

/**
 * @route GET api/article/like
 * @desc  文章点赞
 * @access 接口是公开的
 */
router.post('/like', passport.authenticate('jwt', { session: false }), async ctx => {
  const artId = ctx.request.body.articleId
  const article = await Article.findById({ _id: mongoose.Types.ObjectId(artId) })
  if (!article) {
    ctx.status = 404
    ctx.body = '文章不存在'
    return
  }
  // console.log('shumu',article.likes.filter(user => user._id.toString() === ctx.state.user.id).length);
  const isLike = article.likes.filter(user => user._id.toString() === ctx.state.user.id).length > 0
  //isLik = true : 已点赞
  if (isLike) {
    const removeIndex = article.likes.map(item => item._id.toString()).indexOf(ctx.state.user.id)
    article.likes.splice(removeIndex, 1)
    await Article.findOneAndUpdate(
      { _id: artId },
      { $set: article },
      { new: true }
    ).then(res => {
      ctx.status = 200
      const count = res.likes.length
      ctx.body = {
        isLike: '取消喜欢',
        count: count,
        likes: res.likes,
        liked: false
      }
    })
  } else {
    article.likes.unshift(ctx.state.user.id)
    await Article.findOneAndUpdate(
      { _id: artId },
      { $set: article },
      { new: true }
    ).then(res => {
      const count = res.likes.length
      ctx.status = 200
      ctx.body = {
        isLike: '收到你的喜欢',
        count: count,
        likes: res.likes,
        liked: true
      }
    })
  }
})

/**
 * @route GET api/article/star
 * @desc  文章收藏
 * @access 接口是公开的
 */
router.post('/star', passport.authenticate('jwt', { session: false }), async ctx => {
  const artId = ctx.request.body.articleId
  const article = await Article.findById({ _id: artId })
  if (isEmpty(article)) {
    ctx.status = 404
    ctx.body = '文章不存在'
    return
  }
  // console.log('shumu',article.likes.filter(user => user._id.toString() === ctx.state.user.id).length);
  const isStar = article.stars.filter(user => user._id.toString() === ctx.state.user.id).length > 0
  // console.log('文章',article)
  if (isStar) {
    const removeIndex = article.stars.map(item => item._id.toString()).indexOf(ctx.state.user.id)
    article.stars.splice(removeIndex, 1)
    await Article.findOneAndUpdate(
      { _id: artId },
      { $set: article },
      { new: true }
    ).then(res => {
      ctx.status = 200
      const count = res.stars.length
      ctx.body = {
        isStar: '取消收藏',
        count: count,
        stars: res.stars,
        stared: false
      }
    })
  } else {
    article.stars.unshift(ctx.state.user.id)
    await Article.findOneAndUpdate(
      { _id: artId },
      { $set: article },
      { new: true }
    ).then(res => {
      const count = res.stars.length
      ctx.status = 200
      ctx.body = {
        isStar: '已收藏',
        count: count,
        stars: res.stars,
        stared: true
      }
    })
  }
})
/**
 * @route POST api/article/toleave
 * @desc  文章留言(评论)
 * @access 接口是公开的
 */
router.post('/toleave', passport.authenticate('jwt', { session: false }), async ctx => {
  const text = ctx.request.body.text
  const artId = ctx.request.body.articleId
  if (text.length > 200) {
    ctx.status = 400
    ctx.body = '留言的字数最多为200'
    return
  }
  const article = await Article.findById({ _id: artId })
  if (!article) {
    ctx.status = 404
    ctx.body = '文章不存在'
  }
  const data = {
    user: ctx.state.user.id,
    text: text,
    name: ctx.state.user.name,
    avatar: ctx.state.user.avatar
  }
  article.comments.push(data)
  await Article.findOneAndUpdate(
    { _id: artId },
    { $set: article },
    { new: true }
  ).then(res => {
    ctx.status = 200
    ctx.body = {
      msg: '评论成功',
      data: res.comments
    }
  })
})
/**
 * @route GET api/article/leave
 * @desc  获取文章留言(评论)
 * @access 接口是公开的
 */
router.get('/leave', passport.authenticate('jwt', { session: false }), async ctx => {
  const limit = Number(ctx.query.limit)
  const page = Number(ctx.query.page)
  const skip = limit * (page - 1)
  const artId = ctx.query.articleId
  const article = Article.findById({ _id: artId })
  if (!article) {
    ctx.status = 404
    ctx.body = '文章不存在'
    return
  }
  await Article.findById({ _id: artId }).select('comments').then(res => {
    ctx.status = 200
    const data = res.comments.slice(skip, limit * page)
    const total = res.comments.length
    const pageCount = Math.ceil((total + limit - 1) / limit)
    ctx.body = {
      data: data,
      page: page,
      limit: limit,
      total: total,
      pageCount: pageCount
    }
  })
})
/**
 * @route POST api/article/del-leave
 * @desc  删除文章留言(评论)
 * @access 接口是公开的
 */
router.delete('/del-leave', passport.authenticate('jwt', { session: false }), async ctx => {
  const cmId = ctx.request.body.commentId
  const artId = ctx.request.body.articleId
  const uId = ctx.request.body.user // 评论信息的用户Id
  let article = Article.findById({ _id: artId })
  if (!article) {
    ctx.status = 404
    ctx.body = '文章不存在'
    return
  }
  // 判断用户是否操作自己的评论
  if (uId !== ctx.state.user.id) {
    ctx.status = 400
    ctx.body = '非法操作'
  }
  // 查找评论是否存在
  await Article.findById({ _id: artId }).select('comments').then(async res => {
    const comm = res.comments.map(item => item._id)
    if (comm.indexOf(cmId) === -1) {
      ctx.status = 404
      ctx.body = '该评论不存在'
    } else {
      const removeIdx = comm.indexOf(cmId)
      console.log('index', removeIdx);
      res.comments.splice(removeIdx, 1)
      article = res
      await Article.findOneAndUpdate(
        { _id: artId },
        { $set: article },
        { new: true }
      ).then(result => {
        ctx.status = 200
        ctx.body = {
          msg: '评论删除成功',
          total: result.comments.length,
          data: result.comments
        }
      })
    }
  })
})

/**
 * @route POST api/p-article
 * @desc  获取用户自己的文章(0: 待审核；1：已审核；2：驳回)
 * @access 接口是公开的
 */
router.post('/p-article', passport.authenticate('jwt', { session: false }), async ctx => {
  if (ctx.request.body.state) {
    ctx.status = 400
    ctx.body = '未传入查询文章的状态'
  }
  const state = ctx.request.body.state
  await Article.find({ user: ctx.state.user.id }).where('audit').eq(state).populate('user', ['_id', 'name', 'avatar', 'motto', 'follow', 'followed']).populate('tags', ['name'])
    .then(res => {
      ctx.status = 200
      ctx.body = res
    })
})

/**
 * @route POST api/p-star-article
 * @desc  获取用户自己的收藏的文章
 * @access 接口是公开的
 */
router.get('/p-star-article', passport.authenticate('jwt', { session: false }), async ctx => {
  await User.find({ _id: ctx.state.user.id }).select('star')
    .populate({
      path: 'article',
      match: { audit: { $eq: 1 } },
      populate: {
        path: 'user', select: ['_id', 'name', 'avatar', 'motto', 'follow', 'followed']
      }
    })
    .populate({
      path: 'article',
      match: { audit: { $eq: 1 } },
      populate: {
        path: 'tags'
      }
    })
    .then(res => {
      ctx.status = 200
      ctx.body = res[0].article
    })
})

// 搜索文章
router.post('/p-search', async ctx => {
  const limit = Number(ctx.request.body.limit)
  const page = Number(ctx.request.body.page)
  const skip = limit * (page - 1)
  const keyword = ctx.request.body.keyword
  const reg = new RegExp(keyword, 'i')
  const pageCount = await Article.count({
    $or: [{ title: { $regex: reg } }]
  })
  await Article.find(
    {
      $or: [{ title: { $regex: reg } }],
      audit: { $eq: 1 }
    }
  ).populate('user', ['_id', 'name', 'avatar', 'motto', 'followed'])
  .populate('tags', ['name']).skip(skip)
    .limit(limit)
    .then(res => {
      ctx.status = 200
      ctx.body = {
        data: res,
        limit: limit,
        page: page,
        pageTotal: pageCount
      }
    })
})

// 根据标签查询
router.post('/tag-search', async ctx => {
  const limit = Number(ctx.request.body.limit)
  const page = Number(ctx.request.body.page)
  const skip = limit * (page - 1)
  const keyword = ctx.request.body.keyword
  const reg = new RegExp(keyword, 'i')
  const pageCount = await Article.count({
    $or: [{ tags: { $in: [keyword] } }]
  })
  await Article.find(
    {
      $or: [{ tags: { $in: [keyword] } }],
      audit: { $eq: 1 }
    },
  ).populate('user', ['_id', 'name', 'avatar', 'motto', 'followed'])
    .populate('tags', ['name']).skip(skip)
    .limit(limit)
    .then(res => {
      ctx.status = 200
      ctx.body = {
        data: res,
        limit: limit,
        page: page,
        pageTotal: pageCount
      }
    })
})


module.exports = router.routes()
