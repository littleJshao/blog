const Router = require('koa-router');
const router = new Router()
const axios = require('axios')
const weatherKey = require('../../config/keys.js').weatherKey
const isEmpty = require('../../validation/isEmpty')

/**
 * @route GET api/weather
 * @desc  获取详情
 * @access 接口是公开的
 */

router.get('/', async ctx => {
  let city = ctx.request.query.city
  let info = ''
  let error = ''
  //未获取到值则默认为“上海”
  if (isEmpty(city)) {
    city = '上海' 
  }
  await axios.get('http://apis.juhe.cn/simpleWeather/query', {
    params: {
      key: weatherKey,
      city
    }
  })
    .then(res => {
      info = res.data
    }).catch(err => {
      error = err
    })

    if(error) {
      ctx.status = 404
      ctx.body = error
    }else {
      ctx.status = 200
      ctx.body = info
    }
})

module.exports = router.routes()