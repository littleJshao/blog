const Router = require('koa-router');
const router = new Router();
const passport = require('koa-passport')
const mongoose = require('mongoose')

//引入模板实例
const Leave = require('../../models/Leave')

/**
 * @route POST api/leave/publish
 * @desc  发布留言
 * @access 接口是公开的
 */
router.post('/publish', passport.authenticate('jwt', { session: false }), async ctx => {
  const title = ctx.request.body.title
  const text = ctx.request.body.text
  const leave = {}
  leave.title = title
  leave.text = text
  leave.userAccount = ctx.state.user.email
  await new Leave(leave).save().then(res => {
    ctx.status = 200
    ctx.body = {
      msg: '感谢您的留言,管理员会尽快查看'
    }
  })
})


module.exports = router.routes()