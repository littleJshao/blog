const Router = require('koa-router');
const router = new Router();
const passport = require('koa-passport')
const mongoose = require('mongoose')

//引入模板实例
const About = require('../../models/About')

/**
 * @route GET api/about
 * @desc  获取关于本站信息
 * @access 接口是公开的
 */
router.get('/about',async ctx => {
  await About.findOne().then(res => {
    ctx.status = 200
    ctx.body = res
  })
})