const Router = require('koa-router');
const router = new Router();

//引入模板实例
const Tag = require('../../models/Tag')

//引入验证
const validateTag = require('../../validation/tag')

/**
 * @route GET api/tags
 * @desc  获取分类
 * @access 接口是公开的
 */
router.get('/tags', async ctx => {
  await Tag.find({},(err,res) => {
    if(err) {
      ctx.status = 500
      ctx.body = err
    } else {
      ctx.status = 200
      ctx.body = res
    }
  })
})

/**
 * @route POST api/posts/article/addTag
 * @desc  增加分类
 * @access 接口是公开的
 */
router.post('/addTags', async ctx => {
  // 验证
  const { errors, isValid } = validateTag(ctx.request.body)

  if (!isValid) {
    ctx.status = 400
    ctx.body = errors
    return
  }

  const findResult = await Tag.find({ name: ctx.request.body.name})
  if (findResult.length > 0) {
    ctx.status = 500
    ctx.body = { result: '已存在该分类' }
  } else {
    const newTag = new Tag({
      name: ctx.request.body.name
    })
    await newTag.save().then(res => {
      ctx.status = 200
      ctx.body = res
    }).catch(err => {
      ctx.status = 5000
      ctx.body = err
    })
  }
})


module.exports = router.routes()
