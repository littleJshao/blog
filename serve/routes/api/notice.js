const Router = require('koa-router');
const router = new Router();
const passport = require('koa-passport')
const mongoose = require('mongoose')

//引入模板实例
const Notice = require('../../models/Notice')

/**
 * @route GET api/notice/list
 * @desc  获取公告
 * @access 接口是公开的
 */
router.get('/list', async ctx => {
  const limit = Number(ctx.query.pageSize)
  const page = Number(ctx.query.pageIndex)
  const skip = limit * (page - 1)
  await Notice.find({}).populate('author', ['name', 'avatar'])
    .limit(limit)
    .skip(skip)
    .then(res => {
      ctx.status = 200
      ctx.body = {
        list: res,
        pageSize: limit,
        pageIndex: page,
        pageTotal: res.length
      }
    })
})


module.exports = router.routes()