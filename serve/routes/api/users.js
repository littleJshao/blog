const Router = require('koa-router')
const router = new Router()
const tools = require('../../config/tools')
const bcrypt = require('bcryptjs') // 密码加密
const jwt = require('jsonwebtoken')
const keys = require('../../config/keys')
const passport = require('koa-passport')
const mongoose = require('mongoose')
const jwt_decode = require('jwt-decode');

//引入User
const User = require('../../models/User')
const Article = require('../../models/article')

//引入验证
const validateRegisterInput = require('../../validation/register')
const validateLoginInput = require('../../validation/login')
const validateForgetPwdInput = require('../../validation/forgetpwd')
const validatateModifyInput = require('../../validation/modify')
const isEmpty = require('../../validation/isEmpty')

/**
 * @route Post api/users/register
 * @desc 注册接口地址
 * @access 公开
*/
router.post('/register', async ctx => {
  //验证
  const {errors,isValid} = validateRegisterInput(ctx.request.body)

  if(!isValid) {
    ctx.status = 400
    ctx.body = errors
    return
  }
  const findResult = await User.find({ email: ctx.request.body.email })
  // console.log('邮箱存在', findResult);
  //判断邮箱是否已经存在
  if (findResult.length > 0) {
    ctx.status = 500
    ctx.body = { email: '邮箱已被占用' }
  } else {
    // 如果邮箱不存在就注册
    const newUser = new User({
      name: ctx.request.body.name,
      email: ctx.request.body.email,
      //密码加密
      password: tools.enbcrypt(ctx.request.body.password)
    })
    //存储到数据库
    await newUser.save().then(user => {
      // console.log(newUser);
      ctx.body = user
    }).catch(err => {
      ctx.status = 500
      ctx.body = err
    })
  }
})

/** 
 * @route Post api/users/login
 * @desc 登录接口地址 返回token
 * @access 公开
*/
router.post("/login", async ctx => {
  //验证
  const { errors, isValid } = validateLoginInput(ctx.request.body)
  if (!isValid) {
    ctx.status = 400
    ctx.body = errors
    return
  }
  //查询
  const findResult = await User.find({ email: ctx.request.body.email })
  const password = ctx.request.body.password
  const user = findResult[0]
  //判断是否查到
  if (findResult.length == 0) {
    ctx.status = 404
    ctx.body = {
      email: '用户不存在'
    }
  } else {
    //查到后 验证密码
    let result = bcrypt.compareSync(password, user.password)
    //验证通过
    if (result) {
      //返回token
      const payload = {
        id: user.id,
        name: user.name,
        avatar: user.avatar,
        email: user.email,
        cover: user.cover,
        motto: user.motto,
        follow: user.follow,
        followed: user.followed,
        star: user.star
      }
      const token = jwt.sign(payload, keys.secretOrkey, { expiresIn: 3600});
      ctx.status = 200
      ctx.body = {
        success: true,
        token: "Bearer " + token
      }
    } else {
      ctx.status = 400
      ctx.body = { password: '密码错误!' }
    }
  }
})

/**
 * @route POST api/users/forgetPwd
 * @desc 忘记密码
 * @access 私密
*/
router.post('/forgetPwd',async ctx => {
  const findResult = await User.findOne({ email: ctx.request.body.email })
  if (findResult) {
    if (!(ctx.request.body.newPassword1 && ctx.request.body.newPassword2)) {
      ctx.status = 400
      ctx.body = '请输入完整内容'
      return
    }
    const newPassword1 = ctx.request.body.newPassword1
    const newPassword2 = ctx.request.body.newPassword2
    if (newPassword1 !== newPassword2) {
      ctx.status = 400
      ctx.body = '两次输入的密码不同'
    } else {
      await User.findOneAndUpdate({ email: ctx.request.body.email }, { password: tools.enbcrypt(newPassword2) }).then(res => {
        ctx.status = 200
        ctx.body = '修改成功'
      })
    }
  } else {
    ctx.status = 400
    ctx.body = '该用户不存在'
  }
})

/**
 * @route POST api/users/modifyPwd
 * @desc 修改用户密码
 * @access 私密
*/
router.post('/modifyPwd', passport.authenticate('jwt', { session: false }), async ctx => {
  // 验证
  // const { errors, isValid } = validatateModifyInput(ctx.request.body)
  // if (!isValid) {
  //   ctx.status = 400
  //   ctx.body = errors
  //   return
  // }
  const oldPassword = ctx.request.body.oldPassword
  const newPassword1 = ctx.request.body.newPassword1
  const newPassword2 = ctx.request.body.newPassword2
  let result = bcrypt.compareSync(oldPassword, ctx.state.user.password)
  if(!result) {
    ctx.status = 400
    ctx.body = '原密码错误'
    return
  }
  if (newPassword1 !== newPassword2) {
    ctx.status = 400
    ctx.body = '两次输入的密码不同'
    return
  }
  await User.findByIdAndUpdate(ctx.state.user.id, { password: tools.enbcrypt(newPassword2)}).then(res => {
    ctx.status = 200
    ctx.body = '修改成功'
  })
})

/**
 * @route POST api/users/modifyUser
 * @desc 修改用户信息（名字，个人简介，封面图片，头像）
 * @access 私密
*/
router.post('/modifyUser', passport.authenticate('jwt', { session: false }), async ctx =>{
  if(ctx.request.body.cover) {
    const coverImg = ctx.request.body.cover ?
      ctx.request.body.cover
      : 'https://common-space.oss-cn-beijing.aliyuncs.com/images/default-personalBg.png'
    await User.findByIdAndUpdate(ctx.state.user.id, { cover: coverImg}).then((res) => {
      ctx.status = 200
      ctx.body = '修改封面成功'
    })
  } else {
    const msg = {}
    if(!ctx.request.body.name) {
      ctx.status = 400
      ctx.body = '请填写名字'
      return
    }
    if (ctx.request.body.avatar) {
      const avatarImg = ctx.request.body.avatar ? ctx.request.body.avatar : 'https://common-space.oss-cn-beijing.aliyuncs.com/avatar/defaultAvatar.jfif'
    }
    const motto = ctx.request.body.motto ? ctx.request.body.motto : ''
    await User.findByIdAndUpdate(ctx.state.user.id, { name: ctx.request.body.name, motto: motto, avatar: ctx.request.body.avatar})
    .then(res => {
      ctx.status = 200
      ctx.body = res
    })
  }
})

/**
 * @route Post api/users/forgetpwd
 * @desc 用户信息接口地址: 忘记密码修改用户密码
 * @access 私密
*/
router.post('/forgetpwd', async ctx => {
  //验证
  const { errors, isValid } = validateForgetPwdInput(ctx.request.body)
  if (!isValid) {
    ctx.status = 400
    ctx.body = errors
    return
  }
  const findResult = await User.find({ email: ctx.request.body.email })
  // console.log('邮箱存在', findResult);
  // 判断邮箱是否已经存在
  if (findResult.length > 0) {
    // 查询
    const newUserInfo = await User.findOneAndUpdate(
      { email: ctx.request.body.email},
      { password: tools.enbcrypt(ctx.request.body.password) }
    )
    ctx.status = 200
    ctx.body = newUserInfo
  } else {
    ctx.status = 500
    ctx.body = { email: '邮箱不存在' }
  }
})


/**
 * @route Get api/users/current
 * @desc 用户信息接口地址 返回用户信息------------------------------------------可能要修改成获取用户信息
 * @access 私密
*/
router.get("/currentUser", passport.authenticate('jwt', { session: false }), async ctx => {
  // console.log('用户', ctx.state);
  ctx.body = {
    id: ctx.state.user.id,
    name: ctx.state.user.name,
    email: ctx.state.user.email,
    avatar: ctx.state.user.avatar,
    cover: ctx.state.user.cover,
    motto: ctx.state.user.motto || '',
    article: ctx.state.user.article,
    followed: ctx.state.user.followed
    // upload: ctx.state.user.upload,
    // follow: ctx.state.user.follow,
    // star: ctx.state.user.star,
    // audit: ctx.state.user.audit
  }
})

/**
 * @route Get api/users/publishNum
 * @desc 获取用户已发布的文章数
 * @access 私密
*/
router.get('/authorPublishedNum', async ctx => {
  await User.findById({_id: ctx.query.userId}).populate('article').then(res => {
    const allArticle = res.article
    const published = allArticle.filter(item => item.audit)
    ctx.body = published.length
  })
})

/**
 * @route Get api/users/focusUser
 * @desc 关注用户
 * @access 私密
*/
router.post('/focusUser', passport.authenticate('jwt', { session: false }), async ctx => {
  const userId = ctx.request.body.userId // 用户的ID
  if(userId === ctx.state.user.id) {
    ctx.status = 400
    ctx.body = '自己不需要关注自己哟，多关注其他人吧'
    return
  }
  const findUser = await User.findById({_id: userId}) // 用户信息
  const my = await User.findById(ctx.state.user.id)
  if( isEmpty(findUser) ) {
    ctx.status = 400
    ctx.body = '用户信息异常或用户不存在'
    return
  }
  // 我是否已关注
  const isFocus = my.follow.filter(i => i.user.toString() === userId).length > 0
  if(isFocus) {// 如果已关注
    const removeIndex = my.follow.map(item => item.user.toString()).indexOf(ctx.state.user.id)
    my.follow.splice(removeIndex, 1)
    // my.follow.filter = my.follow.filter( i => i._id.toString() !== userId)
    await User.findOneAndUpdate( // 更改自己的关注列表
      { _id: ctx.state.user.id },
      { $set: my },
      { new: true }
    ).then(async res => {
      // 移除用户对应的被关注一条
      findUser.followed = findUser.followed.filter( i => i.user.toString() !== ctx.state.user.id)
      await User.findOneAndUpdate(
        { _id: userId},
        {$set: findUser},
        {new: true}
      ).then((result) => {
        ctx.status = 200
        ctx.body = {
          msg: '取消关注成功',
          focused: false,
          followed: result.followed
        }
      })
    })
  } else {
    const newUser = { user: mongoose.Types.ObjectId(userId)}
    my.follow.unshift(newUser)
    await User.findOneAndUpdate(
      { _id: ctx.state.user.id },
      { $set: my },
      { new: true }
    ).then( async res => {
      const newUser = { user: ctx.state.user.id}
      // 增加一条到用户的被关注中
      findUser.followed.push(newUser)
      await User.findOneAndUpdate(
        { _id: userId },
        { $set: findUser },
        { new: true }
      ).then(result => {
        ctx.status = 200
        ctx.body = {
          msg: '关注成功',
          focused: true,
          followed: result.followed
        }
      })
    })
  }
})

/**
 * @route POST api/users/refreshUser
 * @desc 刷新用户信息，token
 * @access 私密
*/
router.post('/refreshUser', passport.authenticate('jwt',{session: false}), async ctx => {
  const decode = jwt_decode(ctx.request.body.token)
  const userId = decode.id
  const findResult = await User.findById({ _id: userId })
  if (findResult.length == 0) {
    ctx.status = 404
    ctx.body = {
      email: '用户不存在'
    }
  } else {
    const user = findResult
    const payload = {
      id: user.id,
      name: user.name,
      avatar: user.avatar,
      email: user.email,
      cover: user.cover,
      motto: user.motto,
      follow: user.follow,
      star: user.star
    }
    const token = jwt.sign(payload, keys.secretOrkey, { expiresIn: 3600 }) // 重新刷新token
    ctx.status = 200
    ctx.body = {
      success: true,
      token: "Bearer " + token,
      userInfo: payload
    }
  }
})

/**
 * @route GET api/users/myFocus
 * @desc 获取我关注的用户
 * @access 私密
*/
router.get('/myFocus', passport.authenticate('jwt', { session: false }), async ctx => {
  await User.find({ _id: ctx.state.user.id }).select('follow')
  .populate({
    path:'follow.user',
    select:['name','email','motto','article','photos','avatar','followed'],
    populate:{ path:'photos', select: ['title']},
  })
  .populate({
    path: 'follow.user',
    select: ['name', 'email', 'motto', 'article', 'photos','avatar','followed'],
    populate:{ path: 'article', select: ['title'], match: { audit: { $eq: 1 } }}
  })
  .then(res => {
    const arr = res[0].follow
    let dataList = arr.map(i => {
      return i.user
    })
    let data = []
    dataList.forEach((item,index) => {
      const {name,email,avatar,motto,_id} = item
      const len1 = item.article.length
      const len2 = item.photos.length
      const followed = item.followed.length
      const temp = {
        _id,
        name,
        avatar,
        email,
        motto,
        followed,
        article: len1,
        photos: len2,
        followed: followed
      }
      data[index] = temp
    })
    ctx.status = 200
    ctx.body = data
  })
})

//搜索用户
router.post('/user-search', async ctx => {
  const limit = Number(ctx.request.body.limit)
  const page = Number(ctx.request.body.page)
  const skip = limit * (page - 1)
  const keyword = ctx.request.body.keyword
  const reg = new RegExp(keyword, 'i')
  const pageCount = await User.count({
    $or: [{ name: { $regex: reg } }]
  })
  await User.find(
    {
      $or: [{ name: { $regex: reg } }]
    }
  ).skip(skip)
    .limit(limit)
    .then(res => {
      ctx.status = 200
      ctx.body = {
        data: res,
        limit: limit,
        page: page,
        pageTotal: pageCount
      }
    })
})
module.exports = router.routes()