const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AboutSchema = new Schema({
  content: {
    type: String,
    required: true
  },
  date:{
    type: Date,
    default: new Date
  }
})

module.exports = About = mongoose.model('about', AboutSchema)