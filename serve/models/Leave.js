const mongoose = require('mongoose')
const Schema = mongoose.Schema

const LeaveSchema = new Schema({
  userAccount: {
    type: String,
    required: true
  },
  title: {
    type: 'string',
    required: true
  },
  text: {
    type: 'string',
    required: true
  },
  date: {
    type: Date,
    default: new Date
  }
})
module.exports = Leave = mongoose.model('leave', LeaveSchema)