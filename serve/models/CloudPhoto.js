const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CloudPhotoSchema = new Schema({
  // 用户
  user: {
    type: String,
    ref: 'users',
    required: true
  },
  // 照片
  photo: {
    type: String,
    required: true
  },
  // 标题
  title: {
    type: String,
    required: true,
    maxlength: 20,
  },
  // 简介
  text: {
    type: String,
    maxlength: 200,
    required: true,
  },
  //喜欢
  likes: [
    {
      user: {
        type: Schema.Types.ObjectId,
        ref: 'users'
      }
    }
  ],
  // 喜欢数量
  likesNum:{
    type: Number,
    default: 0
  },
  date: {
    type: Date,
    default: Date.now,
  },
})

module.exports = CloudPhoto = mongoose.model('cloudphoto', CloudPhotoSchema)