const mongoose = require('mongoose')
const Schema = mongoose.Schema

const NoticeSchema = new Schema({
  title: {
    type: 'string',
    required: true
  },
  text:{
    type: 'string',
    required: true
  },
  date:{
    type: Date,
    default: new Date
  }
})
module.exports = Notice = mongoose.model('notice', NoticeSchema)