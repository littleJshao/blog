const mongoose = require('mongoose')
const Schema = mongoose.Schema
const dayjs = require('dayjs')
const ArticleSchema = new Schema({
  //标题
  title: {
    type: String,
    maxlength: 50,
    trim: true,
    required: true
  },
  //作者
  user:{
    type: String,
    ref:'users',
    required: true
  },
  //推荐
  isHot:{
    type: Boolean,
    default: false
  },
  //文章内容
  text: {
    type: String,
    required: true
  },
  profile :{
    type: String,
    minlength: 1,
    maxlength: 200,
    trim: true,
    required: true
  },
  // 封面图片
  coverImages:[
    {
      type:String
    }],
  //标签
  tags: [{
    type: Schema.Types.ObjectId,
    ref: 'tags'
  }],
  //喜欢
  likes: [
    {
      user: {
        type: Schema.Types.ObjectId,
        ref: 'users'
      }
    }
  ],
  //审核
  audit:{
    type: Number,
    default: 0
  },
  //收藏
  stars: [{
    user: {
      type: Schema.Types.ObjectId,
      ref: 'users'
    }
  }],
  //评论
  comments: [{
    //评论的用户Id
    user: {
      type: Schema.Types.ObjectId,
      ref: 'users'
    },
    //评论内容
    text: {
      type: String,
      maxlength: 200,
      required: true
    },
    //用户名字
    name: {
      type: String,
      required: true
    },
    avatar: {
      type: String
    },
    date: {
      type: Date,
      default: Date.now
    }
  }],
  date:{
    type: Date,
    default: Date.now,
  },
  findTypeId:[
    {
      type: Schema.Types.ObjectId,
      ref: 'finds'
    }
  ]
})

module.exports = Article = mongoose.model('article', ArticleSchema)