const mongoose = require('mongoose')
const Schema = mongoose.Schema

//实例化数据模板
const UserSchema = new Schema({
  //昵称
  name: {
    type: String,
    trim: true,
    maxlength: 15,
    minlength: 1,
    required: true
  },
  //账号
  email: {
    type: String,
    required: true
  },
  // 密码
  password: {
    type: String,
    required: true
  },
  // 头像
  avatar: {
    type: String,
    default: 'https://common-space.oss-cn-beijing.aliyuncs.com/avatar/defaultAvatar.jfif'
  },
  //创建日期
  date: {
    type: Date,
    default: Date.now
  },
  //关注的用户
  follow:[
    {
      user: {
        type: Schema.Types.ObjectId,
        ref:'users'
      }
    }
  ],
  // 被关注
  followed: [{
    user:{
      type: Schema.Types.ObjectId,
      ref:'user'
    }
  }],
  //收藏的文章
  star:[
    {
      article:{
        type:Schema.Types.ObjectId,
        ref:'article'
      }
    }
  ],
  // 发表的文章
  article:[
    {
      type:Schema.Types.ObjectId,
      ref:'article'
    }
  ],
  // 云相册
  photos:[
    {
      type: Schema.Types.ObjectId,
      ref: 'cloudphoto'
    }
  ],
  // 个人简介
  motto:{
    type:String,
    maxlength: 100,
    minlength: 1,
    trim: true
  },
  // 个人背景
  cover:{
    type:String,
    default:'https://common-space.oss-cn-beijing.aliyuncs.com/images/default-personalBg.png'
  }

})

module.exports = User = mongoose.model('users',UserSchema)