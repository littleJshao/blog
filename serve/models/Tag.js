const mongoose = require('mongoose')
const Schema = mongoose.Schema

const TagSchema = new Schema({
  name:{
    type: String,
    minlength: 1,
    maxlength: 10,
    required: true
  }
})

module.exports = Tag = mongoose.model('tags', TagSchema)