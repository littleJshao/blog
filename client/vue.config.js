const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  chainWebpack: config => {
    config.resolve.alias
      .set("@", resolve("src"))
  },
  devServer:{
    proxy: {
      '/api':{
        target: 'http://apis.juhe.cn/simpleWeather/query',
        changeOrigin: true,
        pathRewrite: {
          '^/api':''
        }
      }
    }
  }
}