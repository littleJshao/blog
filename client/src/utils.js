import vm from '@/main'
import dayjs from 'dayjs'

//获取地理信息
export const getLocation = function () {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition, showError)
  } else {
    alert("浏览器不支持地理定位。");
  }
  function showPosition(position) {
    console.log(222, position);
    let lat = position.coords.latitude; //纬度 
    let lag = position.coords.longitude; //经度
    console.log(lat, lag);
    return {
      lat,//纬度
      lag//经度
    }
  }
  //错误信息
  function showError(error) {
    switch (error.code) {
      case error.PERMISSION_DENIED:
        alert("定位失败,用户拒绝请求地理定位");
        break;
      case error.POSITION_UNAVAILABLE:
        alert("定位失败,位置信息是不可用");
        break;
      case error.TIMEOUT:
        alert("定位失败,请求获取用户位置超时");
        break;
      case error.UNKNOWN_ERROR:
        alert("定位失败,定位系统失效");
        break;
    }
  }
}

//时间格式化
// export const dateFormat = function (fmt, date) {
//   let ret;
//   let opt = {
//     "Y+": date.getFullYear().toString(),        // 年
//     "m+": (date.getMonth() + 1).toString(),     // 月
//     "d+": date.getDate().toString(),            // 日
//     "H+": date.getHours().toString(),           // 时
//     "M+": date.getMinutes().toString(),         // 分
//     "S+": date.getSeconds().toString()          // 秒
//     // 有其他格式化字符需求可以继续添加，必须转化成字符串
//   };
//   for (let k in opt) {
//     ret = new RegExp("(" + k + ")").exec(fmt);
//     if (ret) {
//       fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
//     };
//   };
//   return fmt;
// }

//获取当前星期
export const getWeek = function (date) {
  let week = date.getDay()
  switch (week) {
    case 0:
      return '周日'
      break;
    case 1:
      return '周一'
      break
    case 2:
      return '周二'
      break
    case 3:
      return '周三'
      break
    case 4:
      return '周四'
      break
    case 5:
      return '周五'
      break
    case 6:
      return '周六'
      break
  }
}

// 验证
export const Rules = {
  required: value => !!value || '必填',
  name: value => value ? value.length >= 1 && value.length <= 15 || '名字长度不小于1位且不大于15位' : '',
  email: value => {
    const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return pattern.test(value) || '邮箱格式不正确'
  },
  pwd: value => value.length >= 6 && value.length <= 30 || '密码长度不小于6位且不大于30位',
  motto: value => value.length <= 100 || '个人介绍不多于100字',
  text: value => value.length <= 200  || '字数不超过200字',
  title: value => value.length <= 20 || '字数不超过20字'
}

// 处理图片
export const handleFileName = function(file) {
  const point = file.name.lastIndexOf(".")
  let suffix = file.name.substr(point)
  let fileName = file.name.substr(0, point)
  let date = new Date().getTime() // 加上时间戳 防止文件名重复
  let fileNames = `${fileName}_${date}${suffix}`
  return fileNames
}

// 处理时间
export const dateFormat = function(val){
  return dayjs(val).format('YYYY-MM-DD')
}