/**   
 * api接口统一管理
 */
import { get, post, Delete } from './http'

export const getWeather = (address) => get("/weather",address)

export const toLogin = (data) => post('/users/login',data)

export const toRegister = (data) => post('/users/register',data)

/**
 * 修改密码
*/
export const toModifyPwd = (data) => post('/users/forgetpwd',data)

/**
 * 获取分类
*/
export const getTag = () => get('/tag/tags')

/**
 * 发布文章（title,user,text,profile,coverImages,tags）
*/
export const publishArticle = (data) => post('/article/publish',data)

/**
 * 忘记密码(修改密码)（email，oldPassword,newPassword）
*/
export const forgetPwd = (data) => post('/users/forgetPwd', data)
/**
 * 修改密码（oldPassword,newPassword）
*/
export const modifyPwd = (data) => post('/users/modifyPwd', data)

/**
 * 修改个人信息（ avatar,cover,motto,name）
*/

export const modifyUser = (data) => post('/users/modifyUser', data)

/**
 * 获取个人信息 GET
*/
export const getUserInfo = () => get('/users/currentUser')

/**
 * 首页获取文章列表
*/
export const getArticleList = (data) => post('/article/list', data)

/**
 * 文章详情
*/
export const getArticleDetail = (data) => get('/article/detail', data)

/**
 * 获取作者已发布的文章数
*/
export const getAuthorPublishedNum = (data) => get('/users/authorPublishedNum', data)

/**
 * 点赞
*/
export const like = (data) => post('/article/like', data)

/**
 * 收藏
*/
export const star = (data) => post('/article/star', data)

/**
 * 评论
*/
export const toleave = (data) => post('/article/toleave', data)

/**
 * 获取评论
*/
export const getLeave = (data) => post('/article/leave', data)


/**
 * 删除评论
*/
export const delLeave = (data) => Delete('/article/del-leave', data)


/**
 * 获取最新图片
*/
export const getNewPhoto = (data) => post('/cloudphoto/newList', data)

/**
 * 获取最热图片
*/
export const getHotPhoto = (data) => post('/cloudphoto/hotList', data)

/**
 * 点赞图片
*/
export const likePhoto = (data) => post('/cloudphoto/like', data)

/**
 * 发布图片
*/
export const publishPhoto = (data) => post('/cloudphoto/publish', data)

/**
 * 获取个人文章（个人界面）
*/
export const getMyArticle = (data) => post('/article/p-article', data)

/**
 * 关注用户(操作)
*/
export const focusUser = (data) => post('/users/focusUser', data)

/**
 * 获取关注用户信息
*/
export const getMyFocus = () => get('/users/myFocus')

/**
 * 获取我的云相册
*/
export const getMyPhotos = () => get('/cloudphoto/myPhotos')

/**
 * 获取我的手收藏
*/
export const getMyStars = () => get('/article/p-star-article')

/**
 * 获取公告信息
*/
export const getNotices = () => get('/notice/list')

/**
 * 发布留言
*/
export const leave = (data) => post('/leave/publish', data)

/**
 * 搜索用户
*/
export const searchUser = (data) => post('/users/user-search', data)

/**
 * 搜索文章
*/
export const searchArticle = (data) => post('/article/p-search', data)

/**
 * 搜索云相册
*/
export const searchPhoto = (data) => post('/cloudphoto/photo-search', data)

/**
 * 按标签搜索文章
*/
export const searchTag = (data) => post('/article/tag-search', data)