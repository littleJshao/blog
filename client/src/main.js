import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import { MessageBox, Message} from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@mdi/font/css/materialdesignicons.css'
import * as api from '@/request/api'
import * as utils from '@/utils.js'

Vue.config.productionTip = false
Vue.prototype.$ELEMENT = { size: 'small', zIndex: 3000 };

Vue.component(Message.name, Message)
Vue.component(MessageBox.name, MessageBox)
Vue.prototype.$message = Message
Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$msgbox = MessageBox
Vue.prototype.$alert = MessageBox.alert
Vue.prototype.$prompt = MessageBox.prompt
Vue.prototype.$api = api // 所有接口请求
Vue.prototype.$utils = utils


//使用钩子函数对路由进行权限跳转
router.beforeEach((to, from, next) => {
  // console.log('to:',to,'from:',from);
  if(to.meta.title) {
    document.title = `${to.meta.title} | Blog`;
  } else {
    document.title = 'Blog'
  }
  if(to.name === 'create' || to.name === 'people') {
    if(localStorage.mToken) {
      next()
    } else {
      Message({
        message: '还未登录呢',
        type: 'warning'
      })
      next('login')
    }
  }
  next()

  // const role = localStorage.getItem('ms_username');
  // if (!role && to.path !== '/login') {
  //   next('/login');
  // } else if (to.meta.permission) {
  //   // 如果是管理员权限则可进入，这里只是简单的模拟管理员权限而已
  //   role === 'admin' ? next() : next('/403');
  // } else {
  //   // 简单的判断IE10及以下不进入富文本编辑器，该组件不兼容
  //   if (navigator.userAgent.indexOf('MSIE') > -1 && to.path === '/editor') {
  //     Vue.prototype.$alert('vue-quill-editor组件不兼容IE10及以下浏览器，请使用更高版本的浏览器查看', '浏览器不兼容通知', {
  //       confirmButtonText: '确定'
  //     });
  //   } else {
  //     next();
  //   }
  // }
});

const vm = new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

export default vm