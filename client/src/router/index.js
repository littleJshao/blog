import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name:"index",
    redirect: '/index'
  },
  {
    path: '/index',
    name: 'index',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "index" */ "@/views/Index.vue"),
    meta: {
      title: '首页'
    }
  },
  {
    path: '/cloudphotos',
    name: 'cloudphotos',
    component: () => import(/* webpackChunkName: "cloudphotos" */ "@/views/Cloudphotos.vue"),
    meta: {
      title: '云相册',
      keepAlive: true
    }
  },
  // {
  //   path: '/pondzone',
  //   name: 'pondzone',
  //   component: () => import(/* webpackChunkName: "pondzone" */ "@/views/Pondzone.vue"),
  //   meta: {
  //     title: '发现圈子',
  //     keepAlive: true
  //   }
  // },
  {
    path: '/editArticle',
    name: 'edit',
    component: () => import(/* webpackChunkName: "more" */ "@/views/Modify.vue"),
    meta: {
      title: '编辑',
      keepAlive: true
    }
  },
  {
    path: '/more',
    name: 'more',
    component: () => import(/* webpackChunkName: "more" */ "@/views/More.vue"),
    meta: {
      title: '本站',
      keepAlive: true
    }
  },
  {
    path:"/people/:peopleId",
    name:'people',
    component: () => import(/* webpackChunkName: "person" */ "@/views/People.vue"),
  },
  // {
  //   path: "/pondzone/:pondId",
  //   name: 'pondZoneDetail',
  //   component: () => import(/* webpackChunkName: "pondZoneDetail" */ "@/views/PondzoneDetail.vue"),
  // },
  {
    path: "/article",
    name: 'article',
    component: () => import(/* webpackChunkName: "article" */ "@/views/Article.vue"),
  },
  {
    path:'/login',
    name:'login',
    component: () => import(/* webpackChunkName: "login" */ "@/views/Login.vue")
  },
  {
    path:'/create',
    name:'create',
    component: () => import(/* webpackChunkName: "create" */ "@/views/Createarticle.vue"),
    meta: {
      title: '创作'
    }
  },{
    path:'/search',
    name:'search',
    component: () => import(/* webpackChunkName: "search" */ "@/views/Search.vue") ,
    meta: {
      title: '搜索'
    }
  }
]
const router = new VueRouter({
  routes,
  mode:'history'
})

export default router
