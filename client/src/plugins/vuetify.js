import '@mdi/font/css/materialdesignicons.css' 
import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  icons:{
    iconfont: 'mdi'
  },
  theme:{
    themes:{
      light:{
        primary: '#CADBAE',
        secondary: '#B0CCCF',
        accent:'#1E3815',
        error:'#D65A50'
      }
    }
  }
});
