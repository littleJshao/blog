import Vue from 'vue'
import Vuex from 'vuex'
import User from "./modules/User";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    showSearch: false
  },
  getters: {
    showSearch: () => state.showSearch
  },
  mutations: {
    setShowSearch: (state, bool) => {
      state.showSearch = bool
    }
  },
  modules:{
    User
  }
})
