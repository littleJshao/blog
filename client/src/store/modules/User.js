import jwt_decode from 'jwt-decode'
import vm from '@/main'

const  user = {
  namespaced: true,
  state: {
    userInfo:'',
    mToken:''
  },
  getters:{
    userInfo: state => state.userInfo,
    mToken: state => state.mToken
  },
  mutations:{
    setToken: (state,token) => {
      state.mToken = token
      localStorage.setItem("mToken", token);
      const decoded = jwt_decode(token)
      state.userInfo = decoded
    },
    removeToken: (state) => {
      state.mToken = ''
      localStorage.removeItem("mToken")
      state.userInfo = ''
    },
    setUserInfo: (state, data) => {
      state.userInfo = data
    }
  },
  actions:{
    setToken: ({commit}, data) => {
      commit('setToken', data)
    },
    setUserInfo: ({commit},data) => {
      commit('setUserInfo', data)
    },
    getUserInfo: ({commit},root) => {
      vm.$api.getUserInfo().then(res => {
         commit('setUserInfo', res)
      })
    }
  }
}

export default user

